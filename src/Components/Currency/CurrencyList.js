import React from 'react'
import './CurrencyList.css'

const CurrencyList = (props) => {
    const {symbol, rates, baseCurrency, removeThisFromTheList} = props
    let ratesInt = isNaN(rates) ? 0 : rates
    let amountByBase = ratesInt * baseCurrency.currencyAmount
    amountByBase = parseFloat(Math.round(amountByBase * 100) / 100).toFixed(4)
    ratesInt = parseFloat(Math.round(ratesInt * 100) / 100).toFixed(4)
    
    return (
        <div className="card">
            <div className="card-data">
                <h5>
                    <span>{symbol}</span>&nbsp;-&nbsp;
                    <span>{amountByBase}</span>
                </h5>
                <strong><em>{symbol} - desc</em></strong>
                <em>1 {baseCurrency.currencySymbol} = {symbol} {ratesInt}</em>
            </div>
            <div className="card-action">
                <button onClick={(index) => removeThisFromTheList(index)}>X</button>
            </div>
        </div>
    )
  }
  
  export default CurrencyList
