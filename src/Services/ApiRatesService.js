const getLatestRates = async () => {
    return fetch('https://api.exchangeratesapi.io/latest')
      .then(response => response.json())
}

const getRatesByCurrency = async(base) => {
    return fetch(`https://api.exchangeratesapi.io/latest?base=${base}`)
      .then(response => response.json());
}

export default {
    getLatestRates,
    getRatesByCurrency
}