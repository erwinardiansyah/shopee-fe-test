import React, { Component } from 'react';
import { connect } from 'react-redux';
import { simpleAction } from './Actions/simpleAction';
import ApiRatesService from './Services/ApiRatesService'
import CurrencyList from './Components/Currency/CurrencyList'
import Autocomplete from './Helpers/Autocomplete';
import Cleave from 'cleave.js/react';
import './App.css';

class App extends Component {
  constructor (props) {
		super(props)
		this.state = {
      defaultCurrency: {
        currencySymbol  : 'USD',
        currencyAmount   : '10.0000',
        currencyDesc    : 'United States Dollars'
      },
      rates: [],
      intialDisplayedCurrency : ['IDR', 'EUR', 'GBP', 'SGD'],
      displayedCurrency: [],
      showForm: false,
      currencyInput: '',
      errNotFound: '',
      arr_filter: []
    }
  }

  componentDidMount() {
    this.getRatesByCurrency(this.state.defaultCurrency.currencySymbol)
  }

  async getRatesByCurrency(base){
    let data  = await ApiRatesService.getRatesByCurrency(base)
    let rates = Object.entries(data.rates).map(([key, value]) => ({key,value})); 
    this.setState({
      rates: rates
    }, () => {
      let displayedCurrency = this.state.displayedCurrency
      rates.map((item, index) => {
        if (this.state.intialDisplayedCurrency.indexOf(item.key) > -1){
           displayedCurrency.push(item)
        }
        return 0
      })
      this.setState({displayedCurrency})
    })
  }

  showLoader(){
    return (<h3 style={{width: '100%', textAlign: 'center'}}>Retrieving data...</h3>)
  }

  updateBasedRatesAmount(event){
    const rawValue = event.target.rawValue
    let baseCurrency = this.state.defaultCurrency
    let rawValueInt = rawValue === '' ? 0 :rawValue
    baseCurrency.currencyAmount = parseInt(rawValueInt) 
    // should update value state of displayed currencies
    this.setState({baseCurrency},()=>{})
  }

  removeThisFromTheList(index){
    const displayedCurrency = this.state.displayedCurrency
    displayedCurrency.splice(index, 1);
    this.setState({displayedCurrency})
  }

  renderHeader(defaultCurrency){
    const currencySymbol = defaultCurrency.currencySymbol
    const currencyDesc = defaultCurrency.currencyDesc
    const currencyAmount = this.state.defaultCurrency.currencyAmount
    
    return(
      <header>
        <em>{currencySymbol} - {currencyDesc}</em>
        <h5>{currencySymbol}</h5>
        <Cleave 
          value={currencyAmount}
          options={{
            numeral: true,
            numeralDecimalScale: 4
          }}
          onChange={this.updateBasedRatesAmount.bind(this)} />
      </header>
    )
  }

  renderItemList(rates){
    const displayedCurrency = this.state.displayedCurrency
    return(
      <React.Fragment>
        {displayedCurrency.map((item, index) => {
          return(
            <CurrencyList
              key ={`${item}-${index}`}
              symbol  = {item.key}
              rates   = {item.value}
              baseCurrency = {this.state.defaultCurrency}
              removeThisFromTheList={() => this.removeThisFromTheList(index)}
            />
          )
        })}
      </React.Fragment>
    )
  }

  renderFooterAction(){
    const showForm  = this.state.showForm
    const rates     = this.state.rates
    const displayedCurrency = this.state.displayedCurrency
    const defaultCurrencySymbol = this.state.defaultCurrency.currencySymbol
    const arr_filter = [defaultCurrencySymbol]
    
    displayedCurrency.map(item => {
      arr_filter.push(item.key)
      return 0
    })
    const arr_rates = []
    rates.map(item=> {
      if (arr_filter.indexOf(item.key) < 0){
        arr_rates.push(item.key)
      }
      return 0
    })

    
    if (showForm){
      return(
        <React.Fragment>
          <Autocomplete
            suggestions={arr_rates}
            onClick={(value) => this.checkCurrencyAvailability(value)}
            autoFocus={true}
          />
        </React.Fragment>
      )
    } else{
      return(
        <button onClick={()=> this.setState({showForm: true})}>(+) Add More Currencies</button>
      )
    }
  }

  checkCurrencyAvailability(value){
    const rates = this.state.rates
    const displayedCurrency = this.state.displayedCurrency
    rates.map((item, index) => {
      if(item.key === value){
        displayedCurrency.push(item)
      }
      return 0
    })
    this.setState({
      displayedCurrency: displayedCurrency,
      showForm: false
    })
  }

  render() {
    const {defaultCurrency, rates} = this.state
    
    if (rates.length === 0){
      return (
        this.showLoader()
      )
    } else {
      return (
        <React.Fragment>
          {this.renderHeader(defaultCurrency)}
          
          <div className={`list-container`}>
            {this.renderItemList(rates)}
          </div>
          
          <footer>
            {this.renderFooterAction()}
          </footer>
          
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  ...state
 })

const mapDispatchToProps = dispatch => ({
    simpleAction: () => dispatch(simpleAction())
 })

 export default connect(mapStateToProps, mapDispatchToProps)(App);